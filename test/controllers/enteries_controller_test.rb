require 'test_helper'

class EnteriesControllerTest < ActionController::TestCase
  setup do
    @entery = enteries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:enteries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create entery" do
    assert_difference('Entery.count') do
      post :create, entery: { amount: @entery.amount, budget_id: @entery.budget_id, title: @entery.title, type: @entery.type }
    end

    assert_redirected_to entery_path(assigns(:entery))
  end

  test "should show entery" do
    get :show, id: @entery
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @entery
    assert_response :success
  end

  test "should update entery" do
    patch :update, id: @entery, entery: { amount: @entery.amount, budget_id: @entery.budget_id, title: @entery.title, type: @entery.type }
    assert_redirected_to entery_path(assigns(:entery))
  end

  test "should destroy entery" do
    assert_difference('Entery.count', -1) do
      delete :destroy, id: @entery
    end

    assert_redirected_to enteries_path
  end
end
