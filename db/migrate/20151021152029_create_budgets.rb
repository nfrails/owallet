class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.string :title
      t.integer :user_id
      t.float :amount
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps null: false
    end
  end
end
