class CreateEnteries < ActiveRecord::Migration
  def change
    create_table :enteries do |t|
      t.string :title
      t.integer :budget_id
      t.float :amount
      t.integer :type

      t.timestamps null: false
    end
  end
end
