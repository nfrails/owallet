class EnteriesController < ApplicationController
  before_action :set_entery, only: [:show, :edit, :update, :destroy]

  # GET /enteries
  # GET /enteries.json
  def index
    @enteries = Entery.all
  end

  # GET /enteries/1
  # GET /enteries/1.json
  def show
  end

  # GET /enteries/new
  def new
    @entery = Entery.new
    @budget = Budget.find(params[:budget_id])
  end

  # GET /enteries/1/edit
  def edit
  end

  # POST /enteries
  # POST /enteries.json
  def create
    @entery = Entery.new(entery_params)
    @budget = @entery.budget
    respond_to do |format|
      if @budget.balance >= @entery.amount

          if @entery.save
            if @entery.type.eql? 1  #Debit
              @budget.balance += @entery.amount
            else if @entery.type.eql? 2  #Credit
                   @budget.balance -= @entery.amount   #Credit
                 end
            end
            @budget.save
            format.js { render :create, status: :created, location: @entery  }
          else
              format.html { render :new }
              format.json { render json: @entery.errors, status: :unprocessable_entity }
          end
      else
        format.js {
          @status = 'error'
          @message = 'Balance is insufficient for this transaction'
        }
      end


    end
  end
  # PATCH/PUT /enteries/1
  # PATCH/PUT /enteries/1.json
  def update
    respond_to do |format|
      if @entery.update(entery_params)
        format.html { redirect_to @entery, notice: 'Entery was successfully updated.' }
        format.json { render :show, status: :ok, location: @entery }
      else
        format.html { render :edit }
        format.json { render json: @entery.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enteries/1
  # DELETE /enteries/1.json
  def destroy
    @entery.destroy
    respond_to do |format|
      format.html { redirect_to enteries_url, notice: 'Entery was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entery
      @entery = Entery.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entery_params
      params.require(:entery).permit(:title, :budget_id, :amount, :type)
    end
end
