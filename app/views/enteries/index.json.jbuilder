json.array!(@enteries) do |entery|
  json.extract! entery, :id, :title, :budget_id, :amount, :type
  json.url entery_url(entery, format: :json)
end
