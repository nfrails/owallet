class Budget < ActiveRecord::Base
  belongs_to :user
  has_many :enteries
  validates :title, :amount, presence: true
end
